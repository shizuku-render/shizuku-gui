﻿
# include <Siv3D.hpp> // OpenSiv3D v0.3.1
#include <thread>
#include <stdlib.h>
void Main()
{
	Graphics::SetBackground(ColorF(0.8, 0.9, 1.0));
  const Font font(60);

  Image buffer({ 300, 200 }, Palette::Black);
  DynamicTexture tex(buffer);
  TCPServer server;
  bool connected = false;
  server.startAccept(50000);
  while (System::Update())
  {
    Circle(Cursor::Pos(), 60).draw(ColorF(1, 0, 0, 0.5));


    if (server.hasSession())
    {
      connected = true;

      int32_t tmp;
      while (server.read(tmp)) {
        switch (tmp)
        {
        case 1:
          int32_t x, y;
          double r, g, b, a;
          while (!server.read(x));
          while (!server.read(y));
          while (!server.read(r));
          while (!server.read(g));
          while (!server.read(b));
          while (!server.read(a));
          ColorF c(r, g, b, a);
          buffer[{x, y}] = c;
          tex.fill(buffer);
          break;
        case 2:
          int32_t w, h;
          while (!server.read(w));
          while (!server.read(h));
          buffer.resize({ w, h });
          tex = DynamicTexture(buffer);
        }
      }
    }
    else if (connected)
    {
      connected = false;
      server.disconnect();
      server.startAccept(50000);
    }

    if (SimpleGUI::Button(U"レンダリング", { 0, 0 }))
    {
      std::thread([]() {
        system("Shizuku.exe");
      }).detach();
    }
    if (SimpleGUI::Button(U"クリア", { 0, 50 }))
    {
      tex.fill(Palette::Black);
    };
    tex.draw(Vec2{ 250, 200 });
  }
}
